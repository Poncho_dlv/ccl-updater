#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, init_log_file_handler, except_hook
from spike_database.Leagues import Leagues
from spike_database.LightMatches import LightMatches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_utilities.Utilities import Utilities


def main():
    init_log_file_handler("update_ccl")
    spike_logger.info("update_match_history")
    update_match_history()
    spike_logger.info("Update finished")
    sys.exit()


def update_match_history():
    light_match_db = LightMatches()
    platforms = ["pc", "xb1", "ps4"]
    league_name = "Cabalvision Official League"
    rsrc_db = ResourcesRequest()
    league_db = Leagues()

    for platform in platforms:
        try:
            spike_logger.info("Get matches from {} - {}".format(league_name, platform))
            matches = CyanideApi.get_matches([league_name], platform=platform, limit=500)
            if matches is not None and len(matches["matches"]) > 0:
                matches = matches["matches"]
                formatted_matches = []
                for match in matches:
                    match["platform"] = platform
                    light_match_db.add_match(match["uuid"], match)
                    formatted_matches.append({"match": match, "uuid": match["uuid"]})
                spike_logger.info("Save {} matches...".format(len(formatted_matches)))
                Utilities.write_json_matches_into_match_history(formatted_matches)
            else:
                spike_logger.info("No match found")

            platform_id = rsrc_db.get_platform_id(platform)
            league_db.update_league_last_update(league_name, platform_id)
        except Exception as e:
            spike_logger.error(e)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
