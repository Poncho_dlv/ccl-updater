#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import sys

from __init__ import spike_logger, init_log_file_handler, except_hook
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.Leagues import Leagues
from spike_database.LightMatches import LightMatches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi


def main():
    init_log_file_handler("update_ccl")
    spike_logger.info("update_standing")
    update_standing()
    spike_logger.info("Update finished")
    sys.exit()


def update_standing():
    platforms = ["pc", "xb1", "ps4"]
    league_name = "Cabalvision Official League"
    rsrc_db = ResourcesRequest()
    competition_db = Competitions()
    league_db = Leagues()
    # Champion cup 45.1 admin
    chalice_45_1_id = 307739
    chalice_45_id = 303556

    for platform in platforms:
        platform_id = rsrc_db.get_platform_id(platform)
        competition_list = CyanideApi.get_competitions(league_name, 500, platform)
        if competition_list.get("leagues", [{}])[0].get("official") == 1:
            league_id = competition_list.get("leagues", [{}])[0].get("id")
            ccl_season = []
            ccl_playoffs = []
            for competition in competition_list.get("competitions", []):
                check_concede = False
                if competition.get("format") == "ladder" and any(x in competition.get("name", "").lower() for x in ["ranked", "ccl", "champion"]):
                    ccl_season.append(competition.get("id"))
                    check_concede = True
                elif competition.get("format") == "single_elimination" and any(x in competition.get("name", "").lower() for x in ["ccl", "champion"]):
                    if platform_id == 1 and competition.get('id') == chalice_45_1_id:
                        ccl_playoffs.append(chalice_45_id)
                    else:
                        ccl_playoffs.append(competition.get("id"))

                if competition.get("status") == 1 and competition.get("format") == "ladder":
                    try:
                        standing = []
                        top_races = {}
                        top_rank = {}
                        competition_db.add_or_update_competition(competition.get("name"), competition.get("id"), league_id, platform_id, competition_format=competition.get("format"))
                        ladder = CyanideApi.get_ladder(league_name, competition.get("name"), 10000, platform)

                        for team in ladder.get("ranking", []):
                            sort = round(team.get("team", {}).get("score") / 10000, 2)
                            wdl = team.get("team", {}).get("w/d/l", "").split("/")
                            win = int(wdl[0])
                            draw = int(wdl[1])
                            loss = int(wdl[2])
                            rank_win = compute_rank(win + 1, draw, loss)
                            rank_draw = compute_rank(win, draw + 1, loss)
                            rank_loss = compute_rank(win, draw, loss + 1)
                            team_data = {
                                "rank": team.get("team", {}).get("rank"),
                                "coach_name": team.get("coach", {}).get("name"),
                                "coach_id": team.get("coach", {}).get("id"),
                                "team_name": team.get("team", {}).get("name"),
                                "team_id": team.get("team", {}).get("id"),
                                "logo": team.get("team", {}).get("logo"),
                                "race": team.get("team", {}).get("race"),
                                "sort": sort,
                                "win": win,
                                "draw": draw,
                                "loss": loss,
                                "rank_win": rank_win,
                                "rank_draw": rank_draw,
                                "rank_loss": rank_loss,
                                "win_behind_first": 0,
                                "games_played": int(wdl[0]) + int(wdl[1]) + int(wdl[2]),
                                "value": team.get("team", {}).get("tv"),
                            }

                            if top_rank.get(team.get("team", {}).get("race")) is None:
                                top_rank[team.get("team", {}).get("race")] = team_data["sort"]
                            else:
                                # compute win behind first
                                rank = team_data["sort"]
                                add_win = 0
                                while rank < top_rank[team.get("team", {}).get("race")]:
                                    add_win += 1
                                    rank = compute_rank(win + add_win, draw, loss)
                                team_data["win_behind_first"] = add_win

                            standing.append(team_data)

                            if top_races.get(team.get("team", {}).get("race")) is None:
                                top_races[team.get("team", {}).get("race")] = []
                            top_races[team.get("team", {}).get("race")].append(team_data)

                        competition_id = ladder.get("ladder", {}).get("competition_id")
                        competition_db.set_standing(competition_id, platform_id, standing, "blood_bowl_2_standing", "rank")
                        competition_db.set_top_races(competition_id, platform_id, top_races)

                        if check_concede:
                            over_5_concede = build_ban_list(competition_id, platform)
                            active_ban = []
                            inactive_ban = []

                            for coach in over_5_concede:
                                match = next((item for item in standing if item.get("coach_id") == coach["coach_id"]), None)
                                if match is not None:
                                    active_ban.append(coach)
                                else:
                                    inactive_ban.append(coach)
                            competition_db.set_custom_data(competition_id, platform_id, {"active_ban": active_ban, "inactive_ban": inactive_ban})
                    except Exception as e:
                        print(e)
                elif competition.get("status") == 2 and competition.get("format") == "single_elimination":
                    if platform_id == 1 and competition.get('id') == chalice_45_1_id:
                        # Hack replace chalice 45.1 by full 45
                        save_playoffs_stats(303556, platform_id)
                    else:
                        save_playoffs_stats(competition.get("id"), platform_id)
                    competition_db.add_or_update_competition(competition.get("name"), competition.get("id"), league_id, platform_id, competition_format=competition.get("format"))

            if league_id is not None:
                ccl_season.sort(reverse=True)
                ccl_playoffs.sort(reverse=True)
                league_db.set_custom_data(league_id, platform_id, {"ccl_season": ccl_season, "ccl_playoffs": ccl_playoffs})


def save_playoffs_stats(competition_id: int, platform_id: int):
    competition_db = Competitions()
    contest_db = Contests()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data.get("winner") is None and competition_data.get("contest_fully_collected"):
        contests = contest_db.get_contests_in_competition(competition_data.get("id"), platform_id, 2)
        contests = sorted(contests, key=lambda i: i["contest_id"], reverse=True)
        final_contest = contests[0]
        team_home = final_contest.get("team_home")
        team_away = final_contest.get("team_away")
        if team_home is not None and team_away is not None:
            if team_home["score"] > team_away["score"]:
                winner = team_home
                runner_up = team_away
            else:
                winner = team_away
                runner_up = team_home
            winner_data = {
                "winner": winner,
                "runner_up": runner_up
            }
            competition_db.set_custom_data(competition_id, platform_id, winner_data)


def build_ban_list(competition_id: int, platform: str):
    match_db = LightMatches()
    matches = match_db.get_matches_in_competition(competition_id, platform)
    concede_coach = {}
    above_5_concede = []

    for match in matches:
        home_team = match.get("teams", [{}, {}])[0]
        home_coach = match.get("coaches", [{}, {}])[0]
        away_team = match.get("teams", [{}, {}])[1]
        away_coach = match.get("coaches", [{}, {}])[1]
        if home_team.get("mvp") == 0:
            if concede_coach.get(home_coach.get("idcoach")) is None:
                concede_coach[home_coach.get("idcoach")] = {
                    "coach_id": home_coach.get("idcoach"),
                    "coach_name": home_coach.get("coachname"),
                    "number": 0,
                    "match": []
                }
            concede_coach[home_coach.get("idcoach")]["number"] += 1
            concede_coach[home_coach.get("idcoach")]["match"].append(match.get("uuid"))

        if away_team.get("mvp") == 0:
            if concede_coach.get(away_coach.get("idcoach")) is None:
                concede_coach[away_coach.get("idcoach")] = {
                    "coach_id": away_coach.get("idcoach"),
                    "coach_name": away_coach.get("coachname"),
                    "number": 0,
                    "match": []
                }
            concede_coach[away_coach.get("idcoach")]["number"] += 1
            concede_coach[away_coach.get("idcoach")]["match"].append(match.get("uuid"))

    for coach in concede_coach.values():
        if coach.get("number", 0) > 5:
            above_5_concede.append(coach)
    return above_5_concede


def compute_rank(win: int, draw: int, loss: int):
    games_played = win + draw + loss
    cross_point = 0.2
    limit = 42
    a = 0.05
    target = 28
    x = math.log(a / (1 - cross_point), 10) / math.log(1 - (target / limit), 10)

    win_pct = 100 * (win + draw / 2) / games_played
    rank_points = win_pct * (cross_point + (1 - cross_point) * (1 - ((1 - (0.5 * ((games_played + limit) - math.sqrt((games_played - limit) ** 2)) / limit)) ** x)))
    rank_points += games_played * 0.02
    rank = round(rank_points, 2)
    return rank


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
